var users = require('../app/users');
var db = require('../app/dbase');
var should = require('should');

describe('users', function () {

    var u_id_1;
    var u_id_2;

    describe('registration', function () {
        // check register user function
        it('should be have #registerUser function', function () {
            users.should.be.have.property('registerUser');
            users.registerUser.should.be.a.Function;
        });

        it('should be added new user1 in system', function (done) {
            users.registerUser("7123455", "hash1", "mochatest1", function (err) {
                should.not.exist(err);
                done();
            });

        });
        it('should be added new user2 in system', function (done) {
            users.registerUser("7123456", "hash2", "mochatest2", function (err) {
                should.not.exist(err);
                done();
            });

        });

        it('user will be add in system', function (done) {
            db.query('SELECT nickname FROM ' + db.tablesList.users + ' WHERE nickname=$1', ["mochatest1"], function (results) {
                should.exist(results);
                done();
            });

        });


    });

    describe('authenticateUser', function () {
        // check authenticate user function
        it('should be have #authenticateUser function', function () {
            users.should.be.have.property('authenticateUser');
            users.authenticateUser.should.be.a.Function;
        });

        it('dont authenticate User in system', function (done) {
            users.authenticateUser("7123455", "hash2", function (err) {
                should.exist(err);
                done();
            });
        });

        it('should be authenicate User1 in system', function (done) {
            users.authenticateUser("7123455", "hash1", function (err, u_id) {
                should.not.exist(err);
                u_id_1 = u_id;
                done();
            });
        });
        it('should be authenicate User2 in system', function (done) {
            users.authenticateUser("7123456", "hash2", function (err, u_id) {
                should.not.exist(err);
                u_id_2 = u_id;
                done();
            });
        });

    });

    describe('send messege', function () {
        // check authenticate user function
        it('should be have #sendMessage function', function () {
            users.should.be.have.property('sendMessage');
            users.sendMessage.should.be.a.Function;
        });

        it('should be sendMessage himself ', function (done) {
            users.sendMessage(u_id_1, u_id_1, "hello mocha", function (err) {
                err.should.be.an.Error;
                done();
            });
        });

        it('should be sendMessage  ', function (done) {
            users.sendMessage(u_id_1, u_id_2, 'hello', function (err) {
                should.not.exist(err);
                done();
            });
        });

    });

    describe('delete all', function () {
        it('should throw exception if user1 exist', function (done) {
            users.registerUser("7123455", "hash1", "mochatest1", function (err) {
                err.should.be.an.Error;

                db.query('DELETE FROM ' + db.tablesList.users + ' WHERE nickname=$1', ["mochatest1"], function () {
                    done();
                });

            });

        });

        it('should throw exception if user2 exist', function (done) {
            users.registerUser("7123456", "hash2", "mochatest2", function (err) {
                err.should.be.an.Error;

                db.query('DELETE FROM ' + db.tablesList.users + ' WHERE nickname=$1', ["mochatest2"], function () {
                    done();
                });

            });

        });
    });
});
