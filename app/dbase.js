var conString = "postgres://snail:snailsql@5.19.210.233/snail";
var pg = require('pg');
var log = require('./logger');

/**
 *  Aliases for tables in PostgreSQL.
 * @type Object
 */
exports.tablesList = {
    users: "users",
    system: "system",
    statuses: "statuses",
    relConfUser: "rel_conf_user",
    dialogTypes: "dialog_types",
    conversations: "conversations",
    conversationsHistory: "conversations_history",
    contacts: "contacts",
    conferences: "conferences",
    conferencesHistory: "conferences_history",
    blackList: "black_list",
    attaches: "attaches",
    attachesTypes: "attaches_types"
};

/**
 *   Send query to database. Process errors and writes
 *  all queries and errors in the log file. After query
 *  release user to connection pool (calling done()).
 *   If query was successful, function call callback
 *  with one argument - query result.
 *  Otherwise throw error.
 *
 * @param query
 * @param params
 * @param callback
 */
exports.query = function(query, params, callback) {

    pg.connect(conString, function(err, client, done) {
        if (err) {
            log.error('could not connect to postgres', err);
        }

        client.query(query, params, function(err, results) {
            if (err) {
                log.error('Query failed: ' + query, err);
                throw err;
            }

            log.info('Query passed: ' + query);
            log.info(query);

            done();

            callback(results);
        });
    });

};