var log = require('./logger');
var users = require('./users');
var pool = require('./pool');

/**
 *   Process incoming requests (SIP messages) and
 *  route them to their logic.
 *
 * @param req - incoming SIP message
 */
exports.routeRequest = function (req, socket) {

    // Debug code. We must receive here SIP messages and parse them...
    // but now, we receive just telnet-text messages, so we route them
    // for test server logic

    // clear request from telnet ends symbols
    req.replace('\r\n', '');

    // prepare all action
    var actions = [
        'register',
        'login',
        'addFriend',
        'confirmFriend',
        'message',
        'logout',
        'getContacts'
    ];

    // route switch
    var requestArgs = req.split(' ');
    log.info('Routing message: ' + requestArgs[0]);

    switch (requestArgs[0]) {
        case actions[0]:
            log.info('Register logic');

            var telephone = requestArgs[1];
            var password = requestArgs[2];
            var nickname = requestArgs[3];

            if (telephone && password && nickname) {
                log.info('Try register user with params: ' + telephone + ' && ' + password);
                users.registerUser(telephone, password, nickname, function (err) {
                    if (err) {
                        log.error('Register user failed', err);
                        console.log(err);
                        socket.write('Reg_fail.\n');
                    } else {
                        socket.write('Reg_done.\n');
                    }
                });
            } else {
                throw new Error('Login, password or nickname of user is empty.');
            }

            break;

        case actions[1]:
            log.info('Login logic');

            var telephone = requestArgs[1];
            var password = requestArgs[2];

            users.authenticateUser(telephone, password, function (err, u_id) {
                if (err) {
                    log.error('Authorization fault for ' + telephone + ' by ' + err);
                    socket.write('Auth_fail.\n');
//                    throw err;
                }

                log.info('Auth user callback u_id : ' + u_id);
                pool.addConnection(u_id, socket);
                socket.write('Auth_' + u_id + '.\n');
            });

            break;

        case actions[2]:
            log.info('addFriend logic');
            break;

        case actions[3]:
            log.info('confirmFriend logic');
            break;

        case actions[4]:
            log.info('send message logic');

            var from_user = requestArgs[1];
            var to_user = requestArgs[2];
            var msg = '';
            for (var i = 3; i < requestArgs.length; i++) {
                msg = msg + requestArgs[i] + ' ';
            }

            users.sendMessage(from_user, to_user, msg, function (err) {
                if (err) {
                    log.error('Message doesnt send : ' + err);
//                    socket.write('false\n');
                    return;
                }
//                socket.write('true\n');
            });
            break;

        case actions[5]:
            log.info('logout logic');
            pool.getUIDBySocket(socket, function(u_id, err) {
                if(err) {
                    log.error(err);
                    return;
                }

                pool.removeSocketByUID(u_id, function() {
                    log.info('user ' + u_id + ' logout');
                });
            });
            break;

        case actions[6]:
            log.info('getContacts logic');
            var u_id = requestArgs[1];
            users.getContactsListByUserId(u_id, function(res) {
                res ? socket.write('ContactsJSON_' + JSON.stringify(res.rows) + '\n') :
                      socket.write('ContactsFailed');
            });

            break;


        default:
            log.info('Undefined action');
    }

};