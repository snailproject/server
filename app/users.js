var log = require('./logger');
var db = require('./dbase');
var moment = require('moment');
var pool = require('./pool');

/**
 *   Check user in users table and call callback with checking result -
 *  true if user exists in data base and false otherwise.
 *
 * @param userPhone - checking user phone
 * @param callback
 */
function checkUserExists(userPhone, callback) {
    log.info('checkUserExists called');

    db.query('SELECT telephone FROM ' + db.tablesList.users + ' WHERE telephone = $1', [userPhone], function (result) {
        callback(result.rowCount !== 0);
    });
}

/**
 *   Check conversation between user1 and user2 exist and call callback with id conversation
 *  if it exist and with null otherwise.
 *
 * @param user1
 * @param user2
 * @param callback
 */
function checkConversationExists(user1, user2, callback) {
    log.info('checkConversationExists called');

    db.query('SELECT id FROM ' + db.tablesList.conversations +
            ' WHERE (user_1 = $1 AND user_2 =  $2) OR (user_2 = $3 AND user_1 = $4)',
        [user1, user2, user1, user2], function (result) {
            callback((result.rowCount !== 0 ? result.rows[0].id : null));
        });
}

/**
 *  Authorization users
 *
 * @param userPhone
 * @param passHash
 * @param callback - if authorization done, callback will call
 */
exports.authenticateUser = function (userPhone, passHash, callback) {
    var query = 'SELECT id, password AS pass FROM ' + db.tablesList.users +
        ' WHERE telephone = $1';
    db.query(query, [userPhone], finish);

    function finish(result) {
        var err = (result.rowCount !== 1 ?
            new Error('Something went wrong... rowCount = ' + result.rowCount) : null);

        if (err === null && result.rows[0].pass.toString() !== passHash.toString().trim())
            err = new Error('User with the current login/password does not exist');

        if ('undefined' !== typeof callback) {
            callback(err, result.rows[0].id);
            return;
        }

        if (err) throw err;
    };
};

/**
 *  Add new user in system.
 *
 * @param userPhone
 * @param passHash
 * @param nickname
 * @param callback - only argument is error object (or null).
 *
 * @return boolean - true, if user added in table
 *  and false otherwise.
 */
exports.registerUser = function (userPhone, passHash, nickname, callback) {
    checkUserExists(userPhone, function (isExists) {
        if (isExists) {
            log.info('Cant create user ' + userPhone + ' - user already exists!');
            if ('undefined' !== typeof callback) {
                callback(new Error('User already exists'));
            }
            return;
        }

        var sql = 'INSERT INTO ' + db.tablesList.users + ' (telephone, password, nickname) VALUES ($1, $2, $3);';
        db.query(sql, [userPhone, passHash, nickname], function (result) {
            log.info('User created ' + userPhone);
            var err = result.rowCount === 1 ? null : new Error('Something went wrong, inserted columns ' + result.rowCount);

            if ("undefined" !== typeof callback) {
                callback(err);
                return;
            }

            if (err) throw err;
        });
    });
};

/**
 *   Send message to a user.
 *
 *
 * @param from
 * @param to
 * @param message
 */
exports.sendMessage = function (from, to, message, callback) {
    if (from === to) {
        log.info('User ' + from + ' is foreveralone xD');
        var err = new Error('Forever alone... User shouldnt send message himself.');

        if ('undefined' !== typeof callback) {
            callback(err);
            return;
        }

        if (err) throw err;
    }

    checkConversationExists(from, to, function (convId) {
        if (convId === null) {
            var sql = 'INSERT INTO ' + db.tablesList.conversations + '(user_1, user_2) VALUES ($1, $2);';
            db.query(sql, [from, to], function (result) {
                log.info('Conversation created');
                var err = result.rowCount === 1 ? null : new Error('Something went wrong, inserted columns ' + result.rowCount);

                if (err) throw err;
            });
        }

        // send message directly to user
        pool.getSocketByUID(to, function(socket, err) {
            if (err) {
                log.error(err);
                return;
            }
            socket.write('message ' + from + ' ' + to + ' ' + message + '\n');
        });

        // save message in base
        sql = 'INSERT INTO ' + db.tablesList.conversationsHistory +
            '(from_id, conv_id, message, timestamp, delivery_status) VALUES ($1, $2, $3, $4, $5);';
        var time = moment(new Date()).format("YYYY-MM-DD hh:mm:ss+4");
        db.query(sql, [from, convId, message, time, false], function(result) {
            log.info('Message sent');
            var err = result.rowCount === 1 ? null : new Error('Something went wrong, inserted columns ' + result.rowCount);
            if (err) {
                log.error(err);
            }

            if ('undefined' !== typeof callback) {
                callback(err);
                return;
            }

            if (err) throw err;
        })
    });
}

/**
 *   Add friend with friendId for user with userId.
 *  Note, that success flag after this will be zero,
 *  so you must call special function after accepting
 *  friendship.
 *
 *  todo: need check if user with friendId already friend of user!
 *
 * @param userId
 * @param friendId
 */
addUserFriend = function(userId, friendId) {
    if (userId === friendId) {
        var err = new Error('User cant be added in friends by himself.');
        log.error('addUserFriend', err);

        throw err;
    }

    var sql = 'INSERT INTO ' + db.tablesList.contacts + ' VALUES ($1, $2, false);';
    db.query(sql, [userId, friendId], function(result) {
        if (result.rowCount !== 1) {
            throw new Error('addUserFriend inserted columns = ' + result.rowCount);
        } else {
            log.info('User ' + userId + ' send friendship request for user ' + friendId + ' successful.');
        }
    });
};

/**
 *   Update friendship status between two users.
 *  There are two cases - friendship request applied, or denied,
 *  that depends from status flag.
 *
 * @param acceptorUserId
 * @param initiatorUserId
 * @param status - true, if friendship applied, otherwise - false.
 */
exports.setFriendshipStatus = function(acceptorUserId, initiatorUserId, status) {
    var sql = 'UPDATE ' + db.tablesList.contacts + ' SET success = ' + (!!status) +
              ' WHERE user_id = $1 AND friend_id = $2;';
    db.query(sql, [initiatorUserId, acceptorUserId], function(result) {
        if (result.rowCount !== 1) {
            throw new Error('setFriendshipStatus inserted columns = ' + result.rowCount);
        } else {
            log.info('User ' + acceptorUserId + ' set friendship status for user ' + initiatorUserId + ' as ' + (!!status));
        }
    });
};

/**
 *  Load all contacts for user with user_id from
 * server.
 *
 * @param user_id
 */
exports.getContactsListByUserId = function(user_id, callback) {
    var sql = 'SELECT id, success, telephone, nickname, status_id FROM ' + db.tablesList.contacts + ' AS CONTACTS' +
              ' LEFT JOIN ' + db.tablesList.users + ' AS USERS ON CONTACTS.friend_id = USERS.id WHERE user_id=$1';

    db.query(sql, [user_id], function(res) {
        log.info('Loaded ' + res.rowCount + ' contacts for user uid = ' + user_id);
        if ('undefined' !== typeof callback) {
            callback(res);
        }
    });
};