var log = require('./logger');

/**
 *  All connections to server in format:
 *   <u_id, socket>.
 * @type {Object}
 */
var connections_pool = {};

var users_pool = {};

/**
 *  Add new connection to pool or replace old
 * if record already exists.
 *
 * @param u_id
 * @param socket
 */
exports.addConnection = function(u_id, socket) {
    connections_pool[u_id] = socket;
    users_pool[socket] = u_id;
    log.info('Save socket for user with uid = ' + u_id);

    console.log(connections_pool);
};

/**
 *  Call callback with socket for user id or error, if
 * record not exist (user not authenticated).
 *
 * @param u_id
 */
exports.getSocketByUID = function(u_id, cb) {
    var socket = connections_pool[u_id];
    var err = ('undefined' !== typeof socket) ? null : new Error('Socket for user ' + u_id + ' not exists.');

    if ('undefined' !== typeof cb) {
        cb(socket, err);
        return;
    }

    if (err) throw err;
};

/**
 *  Call callback with u_id for socket or error, if
 * record not exist (user not authenticated).
 *
 * @param u_id
 */
exports.getUIDBySocket = function(socket, cb) {
    var u_id = users_pool[socket];
    var err = ('undefined' !== typeof u_id) ? null : new Error('UID for socket ' + socket + ' not exists.');

    if ('undefined' !== typeof cb) {
        cb(u_id, err);
        return;
    }

    if (err) throw err;
};

/**
 *  Remove socket from connection pool. If record
 * not exists, nothing was happening.
 *
 * @param u_id
 * @param cb
 */
exports.removeSocketByUID = function(u_id, cb) {
    delete users_pool[connections_pool[u_id]];
    delete connections_pool[u_id];
    if ('undefined' !== typeof cb) cb();
};