var winston = require('winston');

var log = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({timestamp: true}),
        new (winston.transports.File)({ filename: 'app/logs/runtime.log', timestamp: true })
    ]
});

exports.info = function info(message) {
    log.info(message);
};

exports.debug = function debug(message) {
    log.debug(message);
};

exports.error = function error(message, err) {
    log.error(message, err);
};