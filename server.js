var net = require('net');
var log = require('./app/logger');
var conf = require('./app/config');
var router = require('./app/routing');

// users online
var usersOnline = 0;

var server = net.createServer(function(sock) {
    // new connection
    usersOnline++;
    log.info('Incoming connection, online ' + usersOnline + ' users.');

    // set encoding of trasfered data to uft-8
    sock.setEncoding('utf8');

    // set events
    sock.on('data', function(chunk) {
        try {
            router.routeRequest(chunk, sock);
//            sock.write('Request done.\n');
        } catch (err) {
            log.error('Routing failed', err);
            console.log(err);
//            sock.write('Request failed by exception.\n');
        }
    });
    
    sock.on('end', function() {
        log.info('Connection closed');
        usersOnline--;
    });
}).listen((process.env.PORT || conf.app.serverPort), function() {
    log.info('Server: up on ' + (process.env.PORT || conf.app.serverPort));
});